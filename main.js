// Load homepage with top rated movies
$().ready(async function loadHome() {
  loading();
  const response = await fetch(
    `http://api.themoviedb.org/3/movie/popular?api_key=e32dfe71d59b64c805f7703db5acf091&page=1`
  );
  const rs = await response.json();

  $("#head-info").append(`phim được yêu thích gần đây`);
  fillMovies(rs.results);

  appendPopPages(rs.page, rs.total_pages);
});

// Init
const apiKey = "e32dfe71d59b64c805f7703db5acf091";
const api = "https://api.themoviedb.org/3/";
var strSearch = "";

function submitCancel() {
  return false;
}

// Load movies data
async function evtSubmit() {
  strSearch = $("form #search-words").val();
  const reqStr = `${api}search/movie?api_key=${apiKey}&query=${strSearch}&page=1`;

  loading();

  const response = await fetch(reqStr);
  const rs = await response.json();

  $("#head-info").empty();
  $("#head-info").append(
    `các bộ phim được tìm thấy với từ khóa "${strSearch}"`
  );
  fillMovies(rs.results);

  appendPages(rs.page, rs.total_pages);
}

// Loading animation
function loading() {
  $("#list").empty();
  $("#list").append(
    `<img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif" alt="loading" class="mx-auto">`
  );
}

// Fill movies
function fillMovies(movies) {
  $("#list").empty();
  for (const m of movies) {
    loadDetail(m.id);
  }
}

// Load movie detail
async function loadDetail(id) {
  const reqStr = `${api}movie/${id}?api_key=${apiKey}`;
  const response = await fetch(reqStr);
  const movie = await response.json();

  fillMovie(movie);
}

// Fill detail
async function fillMovie(m) {
  $("#list").append(`
    <div class="col-sm-3.5 mx-auto my-2 rounded-lg shadow mb-5 bg-white" id="m-${m.id}" style="width: 18rem;">
        <img
          src="https://image.tmdb.org/t/p/w500/${m.poster_path}"
          class="card-img-top"
          alt="${m.id}"
        />
        <div class="card-body">
          <h5 class="card-title">${m.original_title}</h5>
          <p class="card-text">
            &#9900 rating: ${m.vote_average}
          </p>
          <p class="card-text">
          &#9900 length: ${m.runtime} min(s)
          </p>
          <p class="card-text d-none hid-inf">
          &#9900 release date: ${m.release_date}
          </p>
          <p class="card-text d-none hid-inf" id="actors-${m.id}">
          &#9900 actors/actresses:
          </p>
          <p class="card-text d-none hid-inf" id="directors-${m.id}">
          &#9900 directors:
          </p>
          <p class="card-text d-none hid-inf" id="genres-${m.id}">
          &#9900 genres:
          </p>
          <p class="card-text d-none hid-inf">
          &#9900 overview: <br>${m.overview}
          </p>
          <p class="card-text d-none hid-inf show-reviews-btn">
            <a href="#" onclick="reviews(${m.id})">show reviews...</a>
          </p>
          <button onclick="moreDetail(${m.id})" class="btn btn-success mt-3">more detail</button>
        </div>
      </div>
  `);

  if (m.genres != null) {
    for (const g of m.genres) {
      $(`#genres-${m.id}`).append(` #${g.name}`);
    }
  }

  const reqStr = `${api}movie/${m.id}/credits?api_key=${apiKey}`;
  const response = await fetch(reqStr);
  const mas = await response.json();
  if (mas.cast != null) {
    for (let index = 0; index < mas.cast.length && index < 10; index++) {
      $(`#actors-${m.id}`).append(
        ` <a href="#" onclick="loadActorDetail(${mas.cast[index].id})">${mas.cast[index].name}</a>,`
      );
      if (index == 9 || index == mas.cast.length - 1) {
        $(`#actors-${m.id}`).append(`...`);
      }
    }
  }
  if (mas.crew != null) {
    for (let index = 0; index < mas.crew.length; index++) {
      if (mas.crew[index].department == "Directing") {
        $(`#directors-${m.id}`).append(` .<a>${mas.crew[index].name}</a>`);
      }
    }
  }
}

// Show more detail
function moreDetail(id) {
  $(`#m-${id}`).removeAttr("class");
  $(`#m-${id}`).addClass(
    "mb-5 w-100 rounded-lg shadow bg-white row no-gutters"
  );
  $(`#m-${id} img`).addClass("col-md-4");
  $(`#m-${id} div`).addClass("col-md-8 px-4");
  $(`#m-${id} div .hid-inf`).removeClass("d-none");

  $(`#m-${id}`).removeAttr("onclick");
  $(`#m-${id} div button`).attr("onclick", `lessDetail(${id})`);
  $(`#m-${id} div button`).empty();
  $(`#m-${id} div button`).append("show less");

  $([document.documentElement, document.body]).animate(
    {
      scrollTop: $(`#m-${id}`).offset().top
    },
    500
  );
}

// Show less detail
function lessDetail(id) {
  $(`#m-${id}`).removeAttr("class");
  $(`#m-${id}`).addClass(
    "col-sm-3.5 mx-auto my-2 rounded-lg shadow mb-5 bg-white"
  );
  $(`#m-${id} img`).removeClass("col-md-4");
  $(`#m-${id} div`).removeClass("col-md-8 px-4");
  $(`#m-${id} div .hid-inf`).addClass("d-none");

  $(`#m-${id}`).removeAttr("onclick");
  $(`#m-${id} div button`).attr("onclick", `moreDetail(${id})`);
  $(`#m-${id} div button`).empty();
  $(`#m-${id} div button`).append("more detail");

  $([document.documentElement, document.body]).animate(
    {
      scrollTop: $(`#m-${id}`).offset().top
    },
    500
  );
}

// Find actors
async function evtSubmitActors() {
  strSearch = $("form #search-words").val();
  const reqStr = `${api}search/person?api_key=${apiKey}&query=${strSearch}`;

  loading();

  const response = await fetch(reqStr);
  const actors = await response.json();

  $("#head-info").empty();
  $("#head-info").append(
    `các diễn viên được tìm thấy với từ khóa "${strSearch}"`
  );
  fillActors(actors.results);
}

// Fill actors list
function fillActors(actors) {
  $("#list").empty();
  for (const a of actors) {
    if (a.known_for_department == "Acting") {
      $("#list").append(`
      <button class="card col-sm-3.5 mx-auto my-2 rounded-lg shadow mb-5 bg-white p-0" id="a-${a.id}" onclick="actorsMovies(${a.id})" style="width: 18rem;">
          <img
            src="https://image.tmdb.org/t/p/w500/${a.profile_path}"
            class="card-img-top"
            alt="${a.id}"
          />
          <div class="card-body">
            <h5 class="card-title">${a.name}</h5>
          </div>
        </button>
    `);
    }
  }
}

// Load actors' movies
async function actorsMovies(id) {
  const reqStr = `${api}person/${id}/movie_credits?api_key=${apiKey}`;

  loading();

  const response = await fetch(reqStr);
  const ams = await response.json();

  $("#head-info").empty();
  $("#head-info").append(`các bộ phim của diễn viên`);
  fillMovies(ams.cast);
}

// Load actor's detail
async function loadActorDetail(id) {
  const reqStr = `${api}person/${id}?api_key=${apiKey}`;
  const response = await fetch(reqStr);
  const a = await response.json();

  $("#head-info").empty();
  $("#head-info").append(`thông tin của diễn viên ${a.name}`);
  $("#list").empty();
  $("#list").append(`
    <div class="mb-5 w-100 rounded-lg shadow bg-white row no-gutters" style="width: 18rem;">
        <img
          src="https://image.tmdb.org/t/p/w500/${a.profile_path}"
          class="card-img-top col-md-4"
          alt="${a.id}"
        />
        <div class="card-body col-md-8 px-4">
          <h5 class="card-title">${a.name}</h5>
          <p class="card-text">
            &#9900 biography:<br>${a.biography}
          </p>
          <p class="card-text">
          <a href="#" onclick="actorsMovies(${a.id})">&#9900 orther movies</a>
          </p>
        </div>
      </div>
  `);
}

// Load reviews
async function reviews(id) {
  const reqStr = `${api}movie/${id}/reviews?api_key=${apiKey}`;
  const response = await fetch(reqStr);
  const reviews = await response.json();

  showReviews(reviews.results, id);
}

// Show reviews
function showReviews(reviews, id) {
  $(`#m-${id} div .show-reviews-btn`).addClass("d-none");
  $(`#m-${id}`).append(`
  <div class="p-0 m-0">
  <div class="card-body col-md-12 px-4 hid-inf" id="r-${id}">
  <div>
  <div>
  `);
  if (reviews != null) {
    for (let index = 0; index < reviews.length && index < 5; index++) {
      $(`#r-${id}`).append(`
      <h5 class="card-title">${reviews[index].author}</h5>
          <p class="card-text">
            ${reviews[index].content}
          </p>
          <br>
      `);
    }
  }
  $(`#r-${id}`).append(`
          <p class="card-text">
            <a href="#" onclick="hideReviews(${id})">hide reviews</a>
          </p>
      `);
  $([document.documentElement, document.body]).animate(
    {
      scrollTop: $(`#r-${id}`).offset().top
    },
    500
  );
}

// Hide reviews
function hideReviews(id) {
  $(`#r-${id}`).remove();
  $(`#m-${id} div .show-reviews-btn`).removeClass("d-none");
  $([document.documentElement, document.body]).animate(
    {
      scrollTop: $(`#m-${id}`).offset().top
    },
    500
  );
}

// Append page list
function appendPopPages(p, tp) {
  $("#list").append(`
  <nav class="navbar navbar-light bg-white w-100">
    <ul class="pagination m-0 p-2 pagination-sm bg-white justify-content-center mx-auto">
    </ul>
  </nav>
  `);

  $(".pagination").empty();
  for (let i = 1; i <= tp && i <= 10; i++) {
    if (i == p) {
      $(".pagination").append(
        `<li class="page-item active"><button class="page-link" onclick="loadPopPage(${i})">${i}</button></li>`
      );
    } else {
      $(".pagination").append(
        `<li class="page-item"><button class="page-link" onclick="loadPopPage(${i})">${i}</button></li>`
      );
    }
  }
}
// Load popular movies pages
async function loadPopPage(p) {
  loading();
  const response = await fetch(
    `http://api.themoviedb.org/3/movie/popular?api_key=e32dfe71d59b64c805f7703db5acf091&page=${p}`
  );
  const rs = await response.json();

  fillMovies(rs.results);

  appendPopPages(rs.page, rs.total_pages);
}

function appendPages(p, tp) {
  $("#list").append(`
  <nav class="navbar navbar-light bg-white w-100">
    <ul class="pagination m-0 p-2 pagination-sm bg-white justify-content-center mx-auto">
    </ul>
  </nav>
  `);

  $(".pagination").empty();
  for (let i = 1; i <= tp && i <= 10; i++) {
    if (i == p) {
      $(".pagination").append(
        `<li class="page-item active"><button class="page-link" onclick="loadPage(${i})">${i}</button></li>`
      );
    } else {
      $(".pagination").append(
        `<li class="page-item"><button class="page-link" onclick="loadPage(${i})">${i}</button></li>`
      );
    }
  }
}

async function loadPage(p) {
  loading();
  const response = await fetch(
    `${api}search/movie?api_key=${apiKey}&query=${strSearch}&page=${p}`
  );
  const rs = await response.json();

  fillMovies(rs.results);

  appendPages(rs.page, rs.total_pages);
}
